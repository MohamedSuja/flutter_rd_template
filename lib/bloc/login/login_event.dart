part of 'login_bloc.dart';

@immutable
sealed class LoginEvent {
  const LoginEvent();
}

final class LoginStarted extends LoginEvent {
  final String email;
  final String password;

  const LoginStarted(this.email, this.password);
}

final class LoginLoading extends LoginEvent {
  final String loading;
  const LoginLoading(this.loading);
}

final class LoginSuccess extends LoginEvent {
  final String loginData;

  const LoginSuccess(this.loginData);
}

final class LoginFailure extends LoginEvent {
  final String error;

  const LoginFailure(this.error);
}
