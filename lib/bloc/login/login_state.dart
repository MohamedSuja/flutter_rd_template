part of 'login_bloc.dart';

@immutable
sealed class LoginState {
  final String email;
  final String password;

  LoginState({
    this.email = '',
    this.password = '',
  });

  const LoginState.login({
    required this.email,
    required this.password,
  });

  const LoginState.loading({
    required this.email,
    required this.password,
  });

  const LoginState.success({
    required this.email,
    required this.password,
  });

  const LoginState.failure({
    required this.email,
    required this.password,
  });
}
