import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginState()) {
    on<LoginEvent>(loginEvent);
  }

  FutureOr<void> loginEvent(LoginEvent event, Emitter<LoginState> emit) async {
    var url = Uri.https(
        'http://clean.laravel-staging.club', '/api/login', {'q': '{https}'});

    try {
      if (event is LoginStarted) {
        var response = await http.post(url, body: {
          'email': event.email,
          'password': event.password,
        });

        if (response.statusCode == 200) {
          print('Login Success');
          // emit(LoginSuccess());
        } else {
          //  emit(LoginFailure());
        }
      }
    } catch (e) {
      print('Error: $e');
      //  emit(LoginFailure());
    }
  }
}
