import 'package:flutter/material.dart';
import 'package:flutter_application/utils/dimensions.dart';
import 'package:flutter_application/widgets/button.dart';
import 'package:flutter_application/widgets/text_feild.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignInState();
}

class _SignInState extends State<SignUp> {
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final onPressed = () {
    print("Sign In");
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: SafeArea(
          child: SizedBox(
            //height: getScreenHeight(context),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo
                    const SizedBox(
                      height: 100,
                    ),
                    const Icon(
                      Icons.lock,
                      size: 100,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    const Text(
                      'Please register to continue',
                    ),

                    CustomTextFiled(
                      controller: emailTextController,
                      hintText: "User Name",
                      obscureText: false,
                      marginTop: 10,
                    ),

                    CustomTextFiled(
                      controller: emailTextController,
                      hintText: "Email",
                      obscureText: false,
                      marginTop: 10,
                      marginBottom: 10,
                    ),

                    CustomTextFiled(
                      controller: passwordTextController,
                      hintText: "New Password",
                      obscureText: true,
                      marginBottom: 10,
                    ),

                    CustomTextFiled(
                      controller: passwordTextController,
                      hintText: "Conform Password",
                      obscureText: true,
                      marginBottom: 10,
                    ),

                    CustomButton(
                      onPressed: onPressed,
                      text: "Sign In",
                      marginBottom: 10,
                    ),

                    TextButton(
                        child: const Text(
                          "Go to Sign In",
                          style: TextStyle(color: Colors.blue),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        })
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
