import 'package:flutter/material.dart';
import 'package:flutter_application/screens/home/home.dart';
import 'package:flutter_application/screens/signup/signup.dart';
import 'package:flutter_application/utils/dimensions.dart';
import 'package:flutter_application/widgets/button.dart';
import 'package:flutter_application/widgets/text_feild.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  final onPressSignUp = (BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const SignUp(),
      ),
    );
  };

  final onPressSignIn = (BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const Home(),
      ),
    );
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: SafeArea(
          child: SizedBox(
            //height: getScreenHeight(context),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Column(
                  //    mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo
                    const SizedBox(
                      height: 100,
                    ),
                    const Icon(
                      Icons.lock,
                      size: 100,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    const Text(
                      'Welcome back, sign in to continue',
                    ),

                    CustomTextFiled(
                      controller: emailTextController,
                      hintText: "Email",
                      obscureText: false,
                      marginTop: 10,
                      marginBottom: 10,
                    ),

                    CustomTextFiled(
                      controller: passwordTextController,
                      hintText: "Password",
                      obscureText: true,
                      marginBottom: 10,
                    ),

                    CustomButton(
                      onPressed: () {
                        onPressSignIn(context);
                      },
                      text: "Sign In",
                      marginBottom: 10,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Don't have an account?",
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        TextButton(
                            child: const Text(
                              "Sign Up",
                              style: TextStyle(color: Colors.blue),
                            ),
                            onPressed: () {
                              onPressSignUp(context);
                            })
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
