import 'package:flutter/material.dart';
import 'package:flutter_application/screens/signIn/signin.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(debugShowCheckedModeBanner: false, home: SignIn());
  }
}
