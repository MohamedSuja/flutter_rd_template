import 'package:flutter/material.dart';

class CustomTextFiled extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final bool obscureText;
  final double? marginTop;
  final double? marginBottom;

  const CustomTextFiled(
      {super.key,
      required this.controller,
      required this.hintText,
      required this.obscureText,
      this.marginTop,
      this.marginBottom});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: marginTop ?? 0, bottom: marginBottom ?? 0),
      child: SizedBox(
        height: 50,
        child: TextField(
          controller: controller,
          obscureText: obscureText,
          decoration: InputDecoration(
              hintText: hintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
              ),
              fillColor: Colors.grey.shade200,
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[500])),
        ),
      ),
    );
  }
}
